package selenium.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.SkipException;
import org.testng.annotations.Test;

import selenium.base.BaseTest;

public class BrowserTest extends BaseTest{
	@Test
	public void chromeBrowserTest() {
		System.out.println("---------Open Chrome----------");
		WebDriver driver=createDriver();
		driver.get("http://yahoo.com");
	}
	
	@Test
	public void edgeBrowserTest()
	{
		throw new SkipException("Yet to implement edge browser");
	}
}
