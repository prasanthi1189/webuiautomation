package selenium.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BaseTest {
	WebDriver driver;
	public WebDriver createDriver()
	{
	String currentDir = System.getProperty("user.dir");
	System.setProperty("webdriver.chrome.driver", currentDir + "\\src\\test\\resources\\chromedriver.exe");
	ChromeOptions ops=new ChromeOptions();
	ops.addArguments("--start-maximized");
	driver = new ChromeDriver(ops);
	return driver;
	}
}
